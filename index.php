<?php
include_once "autoload.php";

$cat = new Cat();
$blackWidow = new BlackWidow();

echo $cat->hasFangs("Cat");
echo "<br>";
echo $cat->move();
echo "<br>";
echo $cat->IsViviparous("Cat");
echo "<br>";
echo "<br>";
echo $blackWidow->isSpiderKind();
echo "<br>";
echo $blackWidow->IsViviparous("BlackWidow");
echo "<br>";
echo $blackWidow->climb();
echo "<br>";
echo $blackWidow->sound();
echo "<br>";
echo $blackWidow->hasPoison("Black Widow");
echo "<br>";
