<?php


interface Kind
{
    public function move($animal="animal");

    public function eat($animal="animal");

    public function sound($animal="animal");

    public function swim($animal="animal");

    public function climb($animal="animal");
}