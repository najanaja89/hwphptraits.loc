<?php

abstract class Animal implements Kind
{

    function move($animal="animal")
    {
        return "$animal Moves";
    }

    function eat($animal="animal")
    {
        return "$animal eats";
    }

    function sound($animal="animal")
    {
        return "$animal Sounds";
    }

    function swim($animal="animal")
    {
        return "$animal Swims";
    }

    function climb($animal="animal")
    {
        return "$animal Climbs";
    }
}