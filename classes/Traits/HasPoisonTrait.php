<?php


namespace Traits;


trait HasPoisonTrait
{
    function hasPoison($name)
    {
        return "$name has poison";
    }
}